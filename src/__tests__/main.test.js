/**
 * @jest-environment jsdom
 */

describe('basic arithmetics are performed correctly', () => {
  beforeEach(async () => {
    // Set up dom manually
    document.body.innerHTML = `
    <div id="numbers-input" class="inline-block">
      <input type="text" name="nr-1" id="nr-1" />
      <input type="text" name="nr-2" id="nr-2" />
    </div>
    <div id="operation-input" class="inline-block">
      <input type="radio" name="operation" id="add" checked />
      <input type="radio" name="operation" id="sub" />
      <input type="radio" name="operation" id="mult" />
      <input type="radio" name="operation" id="div" />
      <label for="div">Divison</label><
    </div>
    <button id="calculate">Berechnen</button>
    <div id="output">
      <p id="result"></p>
    </div>`;
    await import('../scripts/main');
  });

  afterEach(() => {
    jest.resetModules();
  });

  function addNumbersAndCheckRadioButton(nr1, nr2, idSelOption) {
    const elNrInput1 = document.getElementById('nr-1');
    elNrInput1.value = nr1;
    const elNrInput2 = document.getElementById('nr-2');
    elNrInput2.value = nr2;
    const elSelAdd = document.querySelector(`#operation-input #${idSelOption}`);
    elSelAdd.checked = true;
  }

  function checkOutput(expectedOutput) {
    const elResult = document.getElementById('result');
    expect(elResult.textContent).toBe(expectedOutput);
  }

  test('adding two numbers correctly', () => {
    // given  - add numbers and select radio button for addition
    addNumbersAndCheckRadioButton(23, 76, 'add');
    // when - click calculate button
    const elCalcBtn = document.getElementById('calculate');
    elCalcBtn.click();
    // then
    checkOutput('Resultat der Rechnung 23+76: 99');
  });

  test('subtracting two numbers correctly', () => {
    // given  - add numbers and select radio button for subtraction
    addNumbersAndCheckRadioButton(12, 45, 'sub');
    // when - click calculate button
    const elCalcBtn = document.getElementById('calculate');
    elCalcBtn.click();
    // then
    checkOutput('Resultat der Rechnung 12-45: -33');
  });

  test('multiplying two numbers correctly', () => {
    // given  - add numbers and select radio button for multiplication
    addNumbersAndCheckRadioButton(33, 47, 'mult');
    // when - click calculate button
    const elCalcBtn = document.getElementById('calculate');
    elCalcBtn.click();
    // then
    checkOutput('Resultat der Rechnung 33*47: 1551');
  });

  test('dividing two numbers correctly', () => {
    // given  - add numbers and select radio button for division
    addNumbersAndCheckRadioButton(85, 5, 'div');
    // when - click calculate button
    const elCalcBtn = document.getElementById('calculate');
    elCalcBtn.click();
    // then
    checkOutput('Resultat der Rechnung 85/5: 17');
  });

  test('input values are removed', () => {
    // given  - add numbers and select radio button for addition
    addNumbersAndCheckRadioButton(1, 2, 'add');
    // when - click calculate button
    const elCalcBtn = document.getElementById('calculate');
    elCalcBtn.click();
    // then
    const elNrInput1 = document.getElementById('nr-1');
    expect(elNrInput1.value).toBe('');
    const elNrInput2 = document.getElementById('nr-2');
    expect(elNrInput2.value).toBe('');
  });

  test('no calculation done if input is missing', () => {
    // given  - add number to first input only and select radio
    // button for addition
    addNumbersAndCheckRadioButton(10, '', 'add');
    // when - click calculate button
    const elCalcBtn = document.getElementById('calculate');
    elCalcBtn.click();
    // then
    checkOutput('');
  });
});
